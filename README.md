# Capo Bluetooth Server

Server enabling control of Capo robot via Bluetooth

Should be used with [Capo Controller](https://bitbucket.org/capo-mbed/capocontroller/overview)

## Panda setup

### Prerequisites

* Python 2.7 with pip
* Protobuf `protoc` executable https://github.com/google/protobuf
* Protobuf runtime for Python https://github.com/google/protobuf/tree/master/python

### Install amber client libraries

Install python common

```bash
$ git clone https://github.com/project-capo/amber-python-common.git
$ cd amber-python-common/
$ . bin/install.sh
```

Install python clients

```bash
$ git clone https://github.com/project-capo/amber-python-clients.git
$ cd amber-python-clients/
$ . bin/install.sh
```

### Installing bluetooth drivers

Bluez

```
$ sudo apt-get install bluez libbluetooth-dev
```

PyBluez, Python BT interface

```
$ pip install pybluez
```

### Install Python Amber libraries

```
$ git clone https://github.com/project-capo/amber-python-common.git
$ cd amber-python-common/bin && ./install.sh
$ cd ../..
$ git clone https://github.com/project-capo/amber-python-clients.git
$ cd amber-python-client
$ . bin/install.sh
$ sudo python setup.py install
```

### Amber drivers sanity check
```
$ cd amber-python-clients/bin && ./roboclaw-example.sh
# provide loopback address as IP (`127.0.0.1`)
```

If the wheels are spinning - everything was installed correctly.


### Enable blutooth discovery

```
$ sudo hciconfig
# <BT_DEVICE>:	Type: BR/EDR  Bus: USB
#	BD Address: <BT_ADDRESS> ...

$ sudo hciconfig <BT_DEVICE> up piscan # TODO: what is `piscan`? ```

### Enable pairing of device

```
$ sudo bluetooth-agent <PIN>
```

#### Caveats

- it seems like RFCOMM channel 1 is unsuitable for connection on Panda board