from bluetooth import *
from amberclient.common import amber_client
from amberclient.roboclaw import roboclaw

TYPE_ACCELEROMETER = 1


def init_bt_service(uuid, port):
    server_sock = BluetoothSocket( RFCOMM )
    server_sock.bind(("",port))
    server_sock.listen(1)

    port = server_sock.getsockname()[1]
    advertise_service(server_sock, "SampleServer", service_id = uuid)
    return server_sock

def init_roboclaw_proxy():
    ip = raw_input('IP (default: 127.0.0.1): ')
    ip = '127.0.0.1' if ip is None or len(ip) == 0 else ip

    client = amber_client.AmberClient(ip)
    return client, roboclaw.RoboclawProxy(client, 0)

def accept_conn(server_sock):
    return server_sock.accept()

def decode(packet):
    ptype = TYPE_ACCELEROMETER
    px = float(packet[3:7])
    py = float(packet[11:15])
    pz = float(packet[19:23])
    return ptype, (px, py, pz)

def process_data(data, proxy):
    proto, (x, y, z) = decode(data)
    print("received: [{} {} {} {}]".format(proto, x, y, z))
    proxy.send_motors_command(300, 300, 300, 300)

def receive_and_process(client_sock, proxy):
    while True:
        data = client_sock.recv(1024)
        if len(data) == 0: break
        process_data(data, proxy)

def stop_motors(proxy):
    proxy.send_motors_command(0, 0, 0, 0)


def main():
    uuid = "c46303c4-22af-4a95-ad09-48568f6d6b4e"
    port = 2
    server_sock = init_bt_service(uuid, port)
    client, proxy = init_roboclaw_proxy()
    try:
        while True:
            print("Waiting for connection on RFCOMM channel %d" % port)

            client_sock, client_info = accept_conn(server_sock)
            print("Accepted connection from ", client_info)

            try:
                receive_and_process(client_sock, proxy)
            except IOError:
                pass

            stop_motors(proxy)
            print("disconnected")

            client_sock.close()

    except KeyboardInterrupt:
        client.terminate_client()
        proxy.terminate_proxy()
        server_sock.close()
        print("all done")

if __name__ == '__main__':
    main()

